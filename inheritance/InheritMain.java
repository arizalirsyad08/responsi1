package inheritance;
import java.util.Scanner;

public class InheritMain {

	public static void main(String[] args) {
        String name, address, nim, hobi;
        int spp, sks, modul;

        Scanner input = new Scanner(System.in);
        System.out.println("-- INPUT DATAMU --");
        System.out.print("Nama   : ");
        name = input.next();
        System.out.print("NIM    : ");
        nim = input.next();
		System.out.print("Alamat : ");
        address = input.next();
		System.out.print("Hobi	 : ");
        hobi = input.next();
        System.out.print("SPP	 : ");
        spp = input.nextInt();
        System.out.print("SKS	 : ");
        sks = input.nextInt();
        System.out.print("Modul  : ");
        modul = input.nextInt();

        Student mahasiswa = new Student(name, address, nim, hobi);
        mahasiswa.identity();
        System.out.println("Total Pembayaran  : "+mahasiswa.hitungPembayaran(spp, sks, modul));
	}

}